public class GameBoard {
    private final char[][] gameBoard = new char[4][4];
    private int movesAvailable;

    public void clearGameBoard() {
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                this.gameBoard[i][j] = ' ';
            }
        }
        this.movesAvailable = 9;
    }

    public void placeSymbol(int line, int column, char symbol) {
        this.gameBoard[line][column] = symbol;
        if (symbol == ' ') {
            this.movesAvailable++;
        } else {
            this.movesAvailable--;
        }
    }

    public char getSymbol(int line, int column) {
        return this.gameBoard[line][column];
    }

    public int getMovesAvailable() {
        return this.movesAvailable;
    }

    public boolean hasWinner(){
        for (int i = 1; i <= 3; i++) {
            if (gameBoard[i][1]!=' ' && gameBoard[i][1] == gameBoard[i][2] && gameBoard[i][2] == gameBoard[i][3] ||
                    gameBoard[1][i]!=' ' && gameBoard[1][i] == gameBoard[2][i] && gameBoard[2][i] == gameBoard[3][i]) {
                return true;
            }
        }
        return (gameBoard[1][1]!=' ' && gameBoard[1][1] == gameBoard[2][2] && gameBoard[2][2] == gameBoard[3][3]) ||
                (gameBoard[1][3]!=' ' && gameBoard[1][3] == gameBoard[2][2] && gameBoard[2][2] == gameBoard[3][1]);
    }

    public char[][] getGameBoard() {
        return gameBoard;
    }
}
