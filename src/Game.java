import java.util.Random;

public class Game {
    private final IO io = new IO();
    private final GameBoard gameBoard = new GameBoard();
    private boolean gameOver = false;
    private Player player1;
    private Player player2;
    private Player currentPlayer;


    public void mainMenu() {
        int menuSelection;
        do {
            menuSelection = io.menuSelect();
            switch (menuSelection) {
                case 1 -> {
                    playersInitialization(true);
                    do {
                        randomizeFirstPlayer();
                        gameBoard.clearGameBoard();
                        start();
                    } while (io.playAgain());
                }
                case 2 -> {
                    playersInitialization(false);
                    do {
                        randomizeFirstPlayer();
                        gameBoard.clearGameBoard();
                        start();
                    } while (io.playAgain());
                }
                case 3 -> {
                    io.displayHowToPlay();
                }
                default -> {
                }
            }
        } while (menuSelection != 4);
        io.displayAbout();
    }


    private void playersInitialization(boolean isAI) {
        this.player1 = new Player(this.io.selectPlayerName(1), false);
        if (isAI) {
            this.player2 = new Player("Computer", true);
        } else {
            this.player2 = new Player(this.io.selectPlayerName(2), false);
        }
    }

    private void randomizeFirstPlayer() {
        Random random = new Random();
        if (random.nextBoolean()) {
            this.player1.setSymbol('X');
            this.player2.setSymbol('0');
            this.currentPlayer = this.player1;
        } else {
            this.player2.setSymbol('X');
            this.player1.setSymbol('0');
            this.currentPlayer = this.player2;
        }
    }

    private void start() {
        Move playerMove;
        this.io.displayGameBoard(gameBoard.getGameBoard());
        while (!gameOver && gameBoard.getMovesAvailable() > 0) {
            System.out.println('\n' + currentPlayer.getName() + "'s turn (" + currentPlayer.getSymbol() + ")");
            if (currentPlayer.isAI()) {
                playerMove = getBestMove(1);
            } else {
                playerMove = this.io.nextMove(gameBoard.getGameBoard());
            }
            gameBoard.placeSymbol(playerMove.getLine(), playerMove.getColumn(), currentPlayer.getSymbol());
            this.io.displayGameBoard(gameBoard.getGameBoard());
            if (gameBoard.hasWinner()) {
                gameOver = true;
                io.displayWinner(currentPlayer.getName());
            } else {
                if (player1.equals(currentPlayer)) {
                    currentPlayer = player2;
                } else {
                    currentPlayer = player1;
                }
            }
        }
        if (gameBoard.getMovesAvailable() == 0) {
            io.displayDraw();
        }
        this.gameOver = false;
    }

    private Move getBestMove(int minMax) {
        Move[] availableMoves = initializeMovesAvailable(minMax);
        char symbol = getCurrentSymbol(minMax);
        for (Move availableMove : availableMoves) {
            gameBoard.placeSymbol(availableMove.getLine(), availableMove.getColumn(), symbol);
            if (gameBoard.hasWinner()) {
                availableMove.setValue(availableMoves.length * minMax);
            } else if (availableMoves.length == 1) {
                availableMove.setValue(0);
            } else {
                availableMove.setValue(getBestMove(minMax * -1).getValue());
            }
            gameBoard.placeSymbol(availableMove.getLine(), availableMove.getColumn(), ' ');
        }
        return getMinOrMaxMove(availableMoves, minMax);
    }



    private Move[] initializeMovesAvailable(int minMax){
        Move[] availableMoves = new Move[gameBoard.getMovesAvailable()];
        for (int i = 0; i < gameBoard.getMovesAvailable(); ) {
            for (int j = 1; j <= 3 ; j++) {
                for (int k = 1; k <=3 ; k++) {
                    if (gameBoard.getSymbol(j,k)==' '){
                        availableMoves[i] = new Move(j,k, -10*minMax);
                        i++;
                    }
                }
            }
        }
        return availableMoves;
    }
    private Move getMinOrMaxMove(Move[] availableMoves, int minMax){
        Move bestMove = availableMoves[0];
        if (minMax==1){
            for (int i=1; i< availableMoves.length; i++){
                if (availableMoves[i].getValue()>bestMove.getValue()){
                    bestMove = availableMoves[i];
                }else if (availableMoves[i].getValue()==bestMove.getValue()){
                    Random random = new Random();
                    if (random.nextBoolean()){
                        bestMove = availableMoves[i];
                    }
                }
            }
        }
        if (minMax==-1){
            for (int i=1; i< availableMoves.length; i++){
                if (availableMoves[i].getValue()<bestMove.getValue()){
                    bestMove = availableMoves[i];
                }else if (availableMoves[i].getValue()==bestMove.getValue()){
                    Random random = new Random();
                    if (random.nextBoolean()){
                        bestMove = availableMoves[i];
                    }
                }
            }
        }
        return bestMove;
    }

    private char getCurrentSymbol(int minMax) {
        if (currentPlayer == player1) {
            if (minMax == 1) {
                return player1.getSymbol();
            } else {
                return player2.getSymbol();
            }
        } else {
            if (minMax == 1) {
                return player2.getSymbol();
            } else {
                return player1.getSymbol();
            }
        }
    }
}

