public class Player {
    private final String name;
    private final boolean isAI;
    private char symbol;

    public Player(String name, boolean isAI) {
        this.name = name;
        this.isAI = isAI;
    }

    public String getName() {
        return this.name;
    }

    public char getSymbol() {
        return this.symbol;
    }


    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public boolean isAI() {
        return this.isAI;
    }
}