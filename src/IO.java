import java.util.List;
import java.util.Scanner;

public class IO {
    private final Scanner scanner = new Scanner(System.in);

    public int menuSelect() {
        displayMenu();
        String mode = this.scanner.nextLine();
        if (List.of("1", "2", "3", "4").contains(mode)) {
            return Integer.parseInt(mode);
        } else {
            System.out.println("Option not available");
            return menuSelect();
        }
    }

    public String selectPlayerName(int player) {
        System.out.println("Insert player " + player + " name: ");
        return this.scanner.nextLine();
    }

    public int selectPosition(String lc) {
        System.out.print("Select " + lc + ":");
        String position = scanner.nextLine();
        if (position.equals("1") || position.equals("2") || position.equals("3")) {
            return Integer.parseInt(position);
        } else {
            System.out.println("Invalid line number. Must be between 1 and 3. Please select again");
            return selectPosition(lc);
        }
    }

    public Move nextMove(char[][] gameBoard) {
        int line = selectPosition("line");
        int column = selectPosition("column");
        while (gameBoard[line][column] != ' ') {
            System.out.println("Position already filled. please try again");
            line = selectPosition("line");
            column = selectPosition("column");
        }
        return new Move(line, column, 0);
    }

    public boolean playAgain() {
        System.out.println("\nPlay Again?\n1.Yes 2.No\n");
        String input = scanner.nextLine();
        if (input.equals("1")) {
            return true;
        } else if (input.equals("2")) {
            return false;
        } else {
            System.out.println("Invalid option, please try again:");
            return playAgain();
        }
    }

    private void displayMenu() {
        System.out.println("1. Player vs Computer");
        System.out.println("2. Player vs Player");
        System.out.println("3. How to play");
        System.out.println("4. Exit");
        System.out.println();
        System.out.print("Please select: ");
    }

    public void displayAbout() {
        System.out.println("Created by Matei Barat, as the first Java project. Thanks for playing!");
    }

    public void displayGameBoard(char[][] gameBoard) {
        System.out.println();
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                System.out.print(gameBoard[i][j]);
                if (j < 3) {
                    System.out.print(" | ");
                }
            }
            System.out.println();
            if (i < 3) {
                System.out.println("---------");
            }
        }
    }

    public void displayWinner(String playerName) {
        System.out.println();
        System.out.println(playerName + " wins!");
        System.out.println();
    }

    public void displayDraw() {
        System.out.println();
        System.out.println("It's a draw!");
        System.out.println();
    }

    public void displayHowToPlay() {
        System.out.println("TicTacToe is a turn based game.\n" +
                "It has 2 player, each one having a symbol assigned (either 'X' or an '0')\n" +
                "Players take turns in placing their symbol on the game board.\n" +
                "First player always starts with 'X'.\n" +
                "The game board is 3X3 matrix as below:\n\n" +
                "   1   2   3\n" +
                "1    |   |  \n" +
                "   ---------\n" +
                "2    |   |  \n" +
                "   ---------\n" +
                "3    |   |  \n\n" +
                "It has 3 rows and 3 columns (numbered from 1 to 3)\n" +
                "The goal of the game is to complete either a column, a raw or a diagonal with your symbol, as in the examples below (there are 8 winning possibilities):\n\n" +
                "   1   2   3\n" +
                "1  x | x | x\n" +
                "   ---------\n" +
                "2    |   |  \n" +
                "   ---------\n" +
                "3    |   |  \n\n" +
                "   1   2   3\n" +
                "1  x |   |  \n" +
                "   ---------\n" +
                "2  x |   |  \n" +
                "   ---------\n" +
                "3  x |   |  \n\n" +
                "   1   2   3\n" +
                "1  x |   |  \n" +
                "   ---------\n" +
                "2    | x |  \n" +
                "   ---------\n" +
                "3    |   | x\n\n" +
                "Signs can not be placed over an existing sign (or outside the game board).\n" +
                "If the game board is filled and there is no winner, it is a draw.\n" +
                "Press Enter to return to main menu");
        scanner.nextLine();
    }
}
